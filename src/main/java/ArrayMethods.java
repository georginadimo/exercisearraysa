import lombok.NoArgsConstructor;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor

public class ArrayMethods {

    static void findPairWithGivenSum(int arr[],int sum)
    {
        Map<Integer, Integer> s = new HashMap<>();
        boolean firstTime = false;
        int counter=0;

        for (int i=0; i<arr.length; ++i)
        {
            int temp = sum-arr[i];

            // checking for condition
            if (temp>=0 && s.containsKey(temp)&& firstTime)
            {
                System.out.printf(" and Pair found at index " + s.get(temp) + " and " + i + " (" + temp +
                        ", "+arr[i]+")");
                counter=1;

            }else if(temp>=0 && s.containsKey(temp)&& !firstTime){
            System.out.printf("Pair found at index " + s.get(temp) + " and " + i + " (" + temp +
                    ", "+arr[i]+")");
                firstTime=true;
                counter=1;
            }
            s.put(arr[i],i);
        }

           if(counter==0) {System.out.println("Pairs not found!");}
    }

}

